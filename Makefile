all: libexample.so executable

libexample.so: example.c
	gcc -g -fPIC -o example.o -c $^
	gcc -g -shared -o $@ example.o

executable: main.c
	gcc -g -o $@ $^ -L./ -lexample
