typedef struct MyClass MyClass;
struct MyClass {
	void (*method)();
};


/**
 * La méthode est dans la bibliothèque, dans le même espace que la structure et
 * que la fonction create_object.
 */
void the_method();

/**
 * Voyez ça comme un constructeur de MyClass.
 */
MyClass create_object();
